using System;

namespace Swallow.Functional
{
    /// <summary>
    ///     A struct encapsulating the "Maybe"-concept; a value that may be set or not.
    /// </summary>
    /// <typeparam name="T">Type of the contained value.</typeparam>
    public struct Maybe<T>
    {
        private readonly T value;
        private readonly bool hasValue;

        private Maybe(T value, bool hasValue)
        {
            this.value = value;
            this.hasValue = hasValue;
        }

        /// <summary>
        /// Gets whether the maybe is Just.
        /// </summary>
        public bool IsJust => hasValue;

        /// <summary>
        /// Gets whether the maybe is Nothing.
        /// </summary>
        public bool IsNothing => !hasValue;

        /// <summary>
        ///     Create a Just value.
        /// </summary>
        /// <param name="value">The value to store</param>
        /// <returns>A maybe containing the given value.</returns>
        public static Maybe<T> Just(T value)
        {
            return new Maybe<T>(value, true);
        }

        /// <summary>
        ///     Create a Nothing.
        /// </summary>
        /// <returns>A maybe containing nothing.</returns>
        public static Maybe<T> Nothing()
        {
            return new Maybe<T>(default, false);
        }

        /// <summary>
        ///     Extract the contained value or return the fallback if the maybe is Nothing.
        /// </summary>
        /// <param name="fallback">Fallback value to use.</param>
        /// <returns>Either the contained value or the given fallback.</returns>
        public T FromJust(T fallback)
        {
            return hasValue ? value : fallback;
        }

        /// <summary>
        ///     Attempt to extract the contained value.
        /// </summary>
        /// <returns>The contained value.</returns>
        /// <exception cref="InvalidOperationException">If the maybe is Nothing.</exception>
        public T FromJust()
        {
            if (!hasValue)
            {
                throw new InvalidOperationException("Maybe.FromJust: Cannot extract Just from Nothing");
            }

            return value;
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return hasValue ? $"Just {value}" : "Nothing";
        }

        /// <summary>
        ///     Map a function on the contained value.
        /// </summary>
        /// <param name="func">The function to apply.</param>
        /// <typeparam name="TOut">Type of the resulting value.</typeparam>
        /// <returns>A Just containing the result of the function if the maybe is Just or Nothing otherwise.</returns>
        public Maybe<TOut> Map<TOut>(Func<T, TOut> func)
        {
            return hasValue ? Maybe<TOut>.Just(func(value)) : Maybe<TOut>.Nothing();
        }

        /// <summary>
        ///     Bind a function on the contained value.
        /// </summary>
        /// <param name="func">The function to apply, resulting in another Maybe.</param>
        /// <typeparam name="TOut">Type of the resulting contained value.</typeparam>
        /// <returns>The result of the function if the maybe is Just or Nothing otherwise.</returns>
        public Maybe<TOut> Bind<TOut>(Func<T, Maybe<TOut>> func)
        {
            return hasValue ? func(value) : Maybe<TOut>.Nothing();
        }

        /// <summary>
        ///     Return the result of a given function or Nothing.
        /// </summary>
        /// <param name="func">The function to evaluate, resulting in another Maybe.</param>
        /// <typeparam name="TOut">Type of the resulting contained value.</typeparam>
        /// <returns>The result of the function if the maybe is Just or Nothing otherwise.</returns>
        public Maybe<TOut> Then<TOut>(Func<Maybe<TOut>> func)
        {
            return hasValue ? func() : Maybe<TOut>.Nothing();
        }
    }
}
