using System.Collections.Generic;
using System.Linq;

namespace Swallow.Functional
{
    /// <summary>
    ///     Useful extension methods regarding Maybes.
    /// </summary>
    public static class MaybeExtensions
    {
        /// <summary>
        ///     Turn a nullable value into either a Nothing or a Just.
        /// </summary>
        /// <param name="value">The value to wrap.</param>
        /// <typeparam name="T">The type of the value.</typeparam>
        /// <returns>Nothing if value is <c>null</c>, Just the value otherwise.</returns>
        public static Maybe<T> ToMaybe<T>(this T value) where T : class
        {
            return value == null ? Maybe<T>.Nothing() : Maybe<T>.Just(value);
        }

        /// <summary>
        ///     Select and unwrap all Justs from a list of Maybes.
        /// </summary>
        /// <param name="maybes">The maybes to filter.</param>
        /// <typeparam name="T">The type wrapped in Maybe.</typeparam>
        /// <returns>A list of all values contained in Just.</returns>
        public static IEnumerable<T> Justs<T>(this IEnumerable<Maybe<T>> maybes)
        {
            return maybes.Where(m => m.IsJust).Select(m => m.FromJust());
        }

        /// <summary>
        ///     Join two levels of Maybe together, producing Just of both levels are Just.
        /// </summary>
        /// <param name="nested">The nested Maybe.</param>
        /// <typeparam name="T">Type of the inner value.</typeparam>
        /// <returns>Just if both levels are Just, Nothing otherwise.</returns>
        public static Maybe<T> Join<T>(this Maybe<Maybe<T>> nested)
        {
            return nested.Bind(Functional.Identity<Maybe<T>>());
        }
    }
}
