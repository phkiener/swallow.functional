using System;

namespace Swallow.Functional
{
    /// <summary>
    ///     Useful helper methods for functions.
    /// </summary>
    public static class Functional
    {
        /// <summary>
        ///     Return a function that always produces the same result.
        /// </summary>
        /// <param name="value">The value to produce.</param>
        /// <typeparam name="TIn">The type given to the function, which will be ignored.</typeparam>
        /// <typeparam name="TOut">The type of the value to produce.</typeparam>
        /// <returns>A function which will always return <c>value</c>.</returns>
        public static Func<TIn, TOut> Const<TIn, TOut>(TOut value)
        {
            return _ => value;
        }

        /// <summary>
        ///     Compose to functions together, feeding the result of <c>first</c> into <c>second</c>.
        /// </summary>
        /// <param name="first">The first function to evaluate.</param>
        /// <param name="second">The function to receive the result of the first function.</param>
        /// <typeparam name="T1">Input type of the first function.</typeparam>
        /// <typeparam name="T2">Output type of the first function and input type of the second function.</typeparam>
        /// <typeparam name="T3">Output type of the second function.</typeparam>
        /// <returns>A function composing first and second together.</returns>
        public static Func<T1, T3> Compose<T1, T2, T3>(this Func<T1, T2> first, Func<T2, T3> second)
        {
            return x => second(first(x));
        }

        /// <summary>
        ///     Returns a function that returns its input when called.
        /// </summary>
        /// <typeparam name="T">Type the function accepts and returns.</typeparam>
        /// <returns>The identity function.</returns>
        public static Func<T, T> Identity<T>() => x => x;

        /// <summary>
        ///     Flips the arguments of a dyadic function.
        /// </summary>
        /// <param name="func">The function whose arguments to flip.</param>
        /// <typeparam name="T1">The first argument of the function.</typeparam>
        /// <typeparam name="T2">The second argument of the function.</typeparam>
        /// <typeparam name="T3">The result of the function.</typeparam>
        /// <returns>A function producing the same result as the given function.</returns>
        public static Func<T2, T1, T3> Flip<T1, T2, T3>(this Func<T1, T2, T3> func)
        {
            return (y, x) => func(x, y);
        }

        /// <summary>
        ///     Curries a function, returning a function taking the arguments as a tuple.
        /// </summary>
        /// <param name="func">The function to curry.</param>
        /// <typeparam name="T1">The first argument.</typeparam>
        /// <typeparam name="T2">The second argument.</typeparam>
        /// <typeparam name="T3">The result of the function.</typeparam>
        /// <returns>The curried function.</returns>
        public static Func<(T1, T2), T3> Curry<T1, T2, T3>(this Func<T1, T2, T3> func)
        {
            return tuple => func(tuple.Item1, tuple.Item2);
        }

        /// <summary>
        ///     Uncurries a function, returning a function taking the arguments one by one.
        /// </summary>
        /// <param name="func">The function to uncurry.</param>
        /// <typeparam name="T1">The first argument.</typeparam>
        /// <typeparam name="T2">The second argument.</typeparam>
        /// <typeparam name="T3">The result of the function.</typeparam>
        /// <returns>The uncurried function.</returns>
        public static Func<T1, T2, T3> Uncurry<T1, T2, T3>(this Func<(T1, T2), T3> func)
        {
            return (a, b) => func((a, b));
        }

        /// <summary>
        ///     Turn an action into a function returning Unit.
        /// </summary>
        /// <param name="action">The action to invoke.</param>
        /// <returns>The resulting function.</returns>
        public static Func<Unit> Do(Action action)
        {
            return () =>
            {
                action.Invoke();
                return Unit.Value;
            };
        }
        /// <summary>
        ///     Turn an action into a function returning Unit.
        /// </summary>
        /// <param name="action">The action to invoke.</param>
        /// <typeparam name="T">The argument for the action.</typeparam>
        /// <returns>The resulting function.</returns>
        public static Func<T, Unit> Do<T>(Action<T> action)
        {
            return x =>
            {
                action(x);
                return Unit.Value;
            };
        }

        /// <summary>
        ///     Partially apply an argument to a function, resulting in a new function.
        /// </summary>
        /// <param name="func">The function to partially apply an argument to.</param>
        /// <param name="x">The partially applied argument.</param>
        /// <typeparam name="T1">The type of the first argument.</typeparam>
        /// <typeparam name="T2">The type of the second argument.</typeparam>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <returns>The new function.</returns>
        public static Func<T2, TResult> Apply<T1, T2, TResult>(this Func<T1, T2, TResult> func, T1 x)
        {
            return y => func(x, y);
        }

        /// <summary>
        ///     Partially apply an argument to a function, resulting in a new function.
        /// </summary>
        /// <param name="func">The function to partially apply an argument to.</param>
        /// <param name="x">The partially applied argument.</param>
        /// <typeparam name="T1">The type of the first argument.</typeparam>
        /// <typeparam name="T2">The type of the second argument.</typeparam>
        /// <typeparam name="T3">The type of the third argument.</typeparam>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <returns>The new function.</returns>
        public static Func<T2, T3, TResult> Apply<T1, T2, T3, TResult>(this Func<T1, T2, T3, TResult> func, T1 x)
        {
            return (y, z) => func(x, y, z);
        }

        /// <summary>
        ///     Partially apply two arguments to a function, resulting in a new function.
        /// </summary>
        /// <param name="func">The function to partially apply an argument to.</param>
        /// <param name="x">The partially applied argument.</param>
        /// <param name="y">The second partially applied argument.</param>
        /// <typeparam name="T1">The type of the first argument.</typeparam>
        /// <typeparam name="T2">The type of the second argument.</typeparam>
        /// <typeparam name="T3">The type of the third argument.</typeparam>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <returns>The new function.</returns>
        public static Func<T3, TResult> Apply<T1, T2, T3, TResult>(this Func<T1, T2, T3, TResult> func, T1 x, T2 y)
        {
            return z => func(x, y, z);
        }

        /// <summary>
        ///     Partially apply an argument to a function, resulting in a new function.
        /// </summary>
        /// <param name="func">The function to partially apply an argument to.</param>
        /// <param name="x">The partially applied argument.</param>
        /// <typeparam name="T1">The type of the first argument.</typeparam>
        /// <typeparam name="T2">The type of the second argument.</typeparam>
        /// <typeparam name="T3">The type of the third argument.</typeparam>
        /// <typeparam name="T4">The type of the fourth argument.</typeparam>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <returns>The new function.</returns>
        public static Func<T2, T3, T4, TResult> Apply<T1, T2, T3, T4, TResult>(
            this Func<T1, T2, T3, T4, TResult> func,
            T1 x)
        {
            return (y, z, w) => func(x, y, z, w);
        }

        /// <summary>
        ///     Partially apply an argument to a function, resulting in a new function.
        /// </summary>
        /// <param name="func">The function to partially apply an argument to.</param>
        /// <param name="x">The partially applied argument.</param>
        /// <param name="y">The second partially applied argument.</param>
        /// <typeparam name="T1">The type of the first argument.</typeparam>
        /// <typeparam name="T2">The type of the second argument.</typeparam>
        /// <typeparam name="T3">The type of the third argument.</typeparam>
        /// <typeparam name="T4">The type of the fourth argument.</typeparam>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <returns>The new function.</returns>
        public static Func<T3, T4, TResult> Apply<T1, T2, T3, T4, TResult>(
            this Func<T1, T2, T3, T4, TResult> func,
            T1 x,
            T2 y)
        {
            return (z, w) => func(x, y, z, w);
        }

        /// <summary>
        ///     Partially apply an argument to a function, resulting in a new function.
        /// </summary>
        /// <param name="func">The function to partially apply an argument to.</param>
        /// <param name="x">The partially applied argument.</param>
        /// <param name="y">The second partially applied argument.</param>
        /// <param name="z">The third partially applied argument.</param>
        /// <typeparam name="T1">The type of the first argument.</typeparam>
        /// <typeparam name="T2">The type of the second argument.</typeparam>
        /// <typeparam name="T3">The type of the third argument.</typeparam>
        /// <typeparam name="T4">The type of the fourth argument.</typeparam>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <returns>The new function.</returns>
        public static Func<T4, TResult> Apply<T1, T2, T3, T4, TResult>(
            this Func<T1, T2, T3, T4, TResult> func,
            T1 x,
            T2 y,
            T3 z)
        {
            return w => func(x, y, z, w);
        }
    }
}
