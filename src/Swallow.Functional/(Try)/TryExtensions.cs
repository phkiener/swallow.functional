using System;

namespace Swallow.Functional
{
    /// <summary>
    ///     Useful extensions regarding the Try monad.
    /// </summary>
    public static class TryExtensions
    {
        /// <summary>
        ///     Lifts a throwing function to the Try monad.
        /// </summary>
        /// <param name="throwingFunc">The throwing function to lift.</param>
        /// <typeparam name="T">The input type for the function.</typeparam>
        /// <typeparam name="TResult">The result type of the function.</typeparam>
        /// <returns>A new function returning a Try, catching all exceptions.</returns>
        public static Func<T, Try<TResult>> Catch<T, TResult>(this Func<T, TResult> throwingFunc)
        {
            return x =>
            {
                try
                {
                    var success = throwingFunc(x);
                    return Try<TResult>.Success(success);
                }
                catch (Exception e)
                {
                    return Try<TResult>.Exception(e);
                }
            };
        }

        /// <summary>
        ///     Lifts a throwing function to the Try monad.
        /// </summary>
        /// <param name="throwingFunc">The throwing function to lift.</param>
        /// <typeparam name="TResult">The result type of the function.</typeparam>
        /// <returns>A new function returning a Try, catching all exceptions.</returns>
        public static Func<Try<TResult>> Catch<TResult>(this Func<TResult> throwingFunc)
        {
            return () =>
            {
                try
                {
                    var success = throwingFunc();
                    return Try<TResult>.Success(success);
                }
                catch (Exception e)
                {
                    return Try<TResult>.Exception(e);
                }
            };
        }

        /// <summary>
        ///     Lifts a throwing function to the Try monad and invokes it, producing a Try.
        /// </summary>
        /// <param name="throwingFunc">The throwing function to lift.</param>
        /// <param name="x">The value to pass to the lifted function.</param>
        /// <typeparam name="T">The input type for the function.</typeparam>
        /// <typeparam name="TResult">The result type of the function.</typeparam>
        /// <returns>The resulting Try.</returns>
        public static Try<TResult> Catch<T, TResult>(this Func<T, TResult> throwingFunc, T x)
        {
            try
            {
                var success = throwingFunc(x);
                return Try<TResult>.Success(success);
            }
            catch (Exception e)
            {
                return Try<TResult>.Exception(e);
            }
        }
    }
}
