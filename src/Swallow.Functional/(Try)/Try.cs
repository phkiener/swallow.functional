using System;

namespace Swallow.Functional
{
    /// <summary>
    ///     A try containing a value or an exception.
    /// </summary>
    /// <remarks>
    ///     As there is a concrete type <c>Exception</c>, the documentation might be confusing.
    ///     A rule of thumb: if it is uppercase Exception, the Exception in this Try is referred to,
    ///     otherwise a lowercase exception describes the type <c>Exception</c>, often in relation
    ///     to words like 'throwing'.
    /// </remarks>
    /// <typeparam name="T">The type that is contained in this Try.</typeparam>
    public class Try<T>
    {
        private readonly T value;
        private readonly Exception exception;

        private Try(T value, Exception exception, bool isSuccess)
        {
            this.value = value;
            this.exception = exception;
            IsSuccess = isSuccess;
        }

        /// <summary>
        /// Gets whether the Try contains a Success.
        /// </summary>
        public bool IsSuccess { get; }

        /// <summary>
        /// Gets whether the Try contains an Exception.
        /// </summary>
        public bool IsException => !IsSuccess;

        /// <summary>
        ///     Creates a new Success with the given value.
        /// </summary>
        /// <param name="value">The value to wrap.</param>
        /// <returns>A Success containing the given value.</returns>
        public static Try<T> Success(T value)
        {
            return new Try<T>(value, null, true);
        }

        /// <summary>
        ///     Creates a new Exception with the given exception.
        /// </summary>
        /// <param name="exception">The exception to wrap.</param>
        /// <returns>An Exception containing the given exception.</returns>
        public static Try<T> Exception(Exception exception)
        {
            return new Try<T>(default, exception, false);
        }

        /// <summary>
        ///     Attempt to get the Success from this Try.
        /// </summary>
        /// <returns>The contained value if this Try is a Success.</returns>
        /// <exception cref="InvalidOperationException">If the Try is an Exception.</exception>
        public T FromSuccess()
        {
            return IsSuccess
                ? value
                : throw new InvalidOperationException("Try.FromSuccess: Cannot extract Success from Exception");
        }

        /// <summary>
        ///     Attempt to get the Success from this Try.
        /// </summary>
        /// <param name="fallback">The fallback value.</param>
        /// <returns>The contained value if this Try is a Success or <c>fallback</c> otherwise.</returns>
        public T FromSuccess(T fallback)
        {

            return IsSuccess
                ? value
                : fallback;
        }

        /// <summary>
        ///     Attempt to get the Exception from this Try.
        /// </summary>
        /// <returns>The contained value if this Try is an Exception.</returns>
        /// <exception cref="InvalidOperationException">If the Try is a Success.</exception>
        public Exception FromException()
        {
            return IsSuccess
                ? throw new InvalidOperationException("Try.FromException: Cannot extract Exception from Success")
                : exception;
        }

        /// <summary>
        ///     Attempt to get the Exception from this Try.
        /// </summary>
        /// <param name="fallback">The fallback value.</param>
        /// <returns>The contained exception if this Try is an Exception or <c>fallback</c> otherwise.</returns>
        public Exception FromException(Exception fallback)
        {
            return IsSuccess
                ? fallback
                : exception;
        }

        /// <inheritdoc />
        public override string ToString() => IsSuccess ? $"Success {value}" : $"Exception {exception.Message}";

        /// <summary>
        ///     Map a function on the contained value.
        /// </summary>
        /// <param name="func">The function to apply.</param>
        /// <typeparam name="TOut">The type of the resulting value.</typeparam>
        /// <returns>A Success containing the function's result or the current Exception otherwise.</returns>
        public Try<TOut> Map<TOut>(Func<T, TOut> func)
        {
            return IsSuccess ? Try<TOut>.Success(func(value)) : Try<TOut>.Exception(exception);
        }

        /// <summary>
        ///     Bind a function on the contained value.
        /// </summary>
        /// <param name="func">The function to apply, resulting in another Try.</param>
        /// <typeparam name="TOut">The type of the resulting contained value.</typeparam>
        /// <returns>The result of the function if the Try is Success or the current Exception otherwise.</returns>
        public Try<TOut> Bind<TOut>(Func<T, Try<TOut>> func)
        {
            return IsSuccess ? func(value) : Try<TOut>.Exception(exception);
        }

        /// <summary>
        ///     Bind a throwing function on the contained value.
        /// </summary>
        /// <param name="throwingFunc">The function to apply, wrapping a thrown exception in an Exception.</param>
        /// <typeparam name="TOut">The type of the resulting contained value.</typeparam>
        /// <returns>The result of the function if the Try is Success or the current Exception otherwise.</returns>
        public Try<TOut> Bind<TOut>(Func<T, TOut> throwingFunc)
        {
            return IsSuccess ? Catch(() => throwingFunc(value)) : Try<TOut>.Exception(exception);
        }

        /// <summary>
        ///     Return the result of a given function or the Exception.
        /// </summary>
        /// <param name="func">The function whose result to return.</param>
        /// <typeparam name="TOut">The type of the resulting contained value.</typeparam>
        /// <returns>The result of the function if the Try is Success or the current Exception otherwise.</returns>
        public Try<TOut> Then<TOut>(Func<Try<TOut>> func)
        {
            return IsSuccess ? func() : Try<TOut>.Exception(exception);
        }

        /// <summary>
        ///     Return the result of a given throwing function or the Exception.
        /// </summary>
        /// <param name="throwingFunc">The function whose result to return, wrapping a thrown exception in an Exception.</param>
        /// <typeparam name="TOut">The type of the resulting contained value.</typeparam>
        /// <returns>The result of the function if the Try is Success or the current Exception otherwise.</returns>
        public Try<TOut> Then<TOut>(Func<TOut> throwingFunc)
        {
            return IsSuccess ? Catch(throwingFunc) : Try<TOut>.Exception(exception);
        }

        private static Try<TOut> Catch<TOut>(Func<TOut> func)
        {
            try
            {
                return Try<TOut>.Success(func());
            }
            catch (Exception e)
            {
                return Try<TOut>.Exception(e);
            }
        }
    }
}
