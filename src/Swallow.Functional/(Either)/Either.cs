using System;

namespace Swallow.Functional
{
    /// <summary>
    ///     An either containing one of two possible values.
    /// </summary>
    /// <remarks>
    ///     By convention, if using the Either to denote a "success or error"-value, use the
    ///     left type as error and the right type as value. This aids the user, as whatever is "right"
    ///     is actually right, i.e. success.
    /// </remarks>
    /// <typeparam name="TLeft"></typeparam>
    /// <typeparam name="TRight"></typeparam>
    public struct Either<TLeft, TRight>
    {
        private readonly TLeft left;
        private readonly TRight right;
        private readonly bool isRight;

        private Either(TLeft left, TRight right, bool isRight)
        {
            this.left = left;
            this.right = right;
            this.isRight = isRight;
        }

        /// <summary>
        /// Gets whether the Either is Right.
        /// </summary>
        public bool IsRight => isRight;

        /// <summary>
        /// Gets whether the Either is Left.
        /// </summary>
        public bool IsLeft => !isRight;

        /// <summary>
        ///     Create a Left, containing the given value.
        /// </summary>
        /// <param name="value">The value to wrap as Left.</param>
        /// <returns>A new Either containing the given value.</returns>
        public static Either<TLeft, TRight> Left(TLeft value)
        {
            return new Either<TLeft, TRight>(value, default, false);
        }

        /// <summary>
        ///     Create a Right, containing the given value.
        /// </summary>
        /// <param name="value">The value to wrap as Right.</param>
        /// <returns>A new Either containing the given value.</returns>
        public static Either<TLeft, TRight> Right(TRight value)
        {
            return new Either<TLeft, TRight>(default, value, true);
        }

        /// <summary>
        ///     Apply a function to the Either, mapping the Right and leaving Left untouched.
        /// </summary>
        /// <param name="func">The function to apply to the Right.</param>
        /// <typeparam name="TRightOut">The resulting Right type.</typeparam>
        /// <returns>An Either containing Left if this is Left or a mapped Right.</returns>
        public Either<TLeft, TRightOut> Map<TRightOut>(Func<TRight, TRightOut> func)
        {
            return isRight ? Either<TLeft, TRightOut>.Right(func(right)) : Either<TLeft, TRightOut>.Left(left);
        }

        /// <summary>
        ///     Apply a function to the Either, mapping the Left and leaving Right untouched.
        /// </summary>
        /// <param name="func">The function to apply to the Left.</param>
        /// <typeparam name="TLeftOut">The resulting Left type.</typeparam>
        /// <returns>An Either containing Right if this is Right or a mapped Left.</returns>
        public Either<TLeftOut, TRight> MapLeft<TLeftOut>(Func<TLeft, TLeftOut> func)
        {
            return isRight ? Either<TLeftOut, TRight>.Right(right) : Either<TLeftOut, TRight>.Left(func(left));
        }

        /// <summary>
        ///     Apply a function to the Either, producing a new Right or leaving Left untouched.
        /// </summary>
        /// <param name="func">The function to apply to the Right.</param>
        /// <typeparam name="TRightOut">The resulting Right type.</typeparam>
        /// <returns>An Either containing Left if this is Left or a new Right.</returns>
        public Either<TLeft, TRightOut> Bind<TRightOut>(Func<TRight, Either<TLeft, TRightOut>> func)
        {
            return isRight ? func(right) : Either<TLeft, TRightOut>.Left(left);
        }

        /// <summary>
        ///     Apply a function to the Either, producing a new Left or leaving Right untouched.
        /// </summary>
        /// <param name="func">The function to apply to the Left.</param>
        /// <typeparam name="TLeftOut">The resulting Left type.</typeparam>
        /// <returns>An Either containing Right if this is Right or a new Left.</returns>
        public Either<TLeftOut, TRight> BindLeft<TLeftOut>(Func<TLeft, Either<TLeftOut, TRight>> func)
        {
            return isRight ? Either<TLeftOut, TRight>.Right(right) : func(left);
        }

        /// <summary>
        ///     Return the result of a given function if this is Right or the Left as-is.
        /// </summary>
        /// <param name="func">The function to whose result to return as Right.</param>
        /// <typeparam name="TRightOut">The resulting Right type.</typeparam>
        /// <returns>An Either containing Left if this is Left or new Right.</returns>
        public Either<TLeft, TRightOut> Then<TRightOut>(Func<Either<TLeft, TRightOut>> func)
        {
            return isRight ? func() : Either<TLeft, TRightOut>.Left(left);
        }

        /// <summary>
        ///     Return the result of a given function if this is Left or the Right as-is.
        /// </summary>
        /// <param name="func">The function to whose result to return as Left.</param>
        /// <typeparam name="TLeftOut">The resulting Left type.</typeparam>
        /// <returns>An Either containing Right if this is Right or new Left.</returns>
        public Either<TLeftOut, TRight> ThenLeft<TLeftOut>(Func<Either<TLeftOut, TRight>> func)
        {
            return isRight ? Either<TLeftOut, TRight>.Right(right) : func();
        }

        /// <summary>
        ///     Unwrap the Left value or return a given fallback.
        /// </summary>
        /// <param name="fallback">The fallback to use if this Either is Right</param>
        /// <returns>The unwrapped Left value.</returns>
        public TLeft FromLeft(TLeft fallback)
        {
            return isRight ? fallback : left;
        }

        /// <summary>
        ///     Attempt to unwrap the Left value.
        /// </summary>
        /// <returns>The unwrapped Left value.</returns>
        /// <exception cref="InvalidOperationException">If this Either is Right.</exception>
        public TLeft FromLeft()
        {
            if (isRight)
            {
                throw new InvalidOperationException("Either.FromLeft: Cannot extract Left from Right");
            }

            return left;
        }

        /// <summary>
        ///     Unwrap the Right value or return a given fallback.
        /// </summary>
        /// <param name="fallback">The fallback to use if this Either is Left</param>
        /// <returns>The unwrapped Right value.</returns>
        public TRight FromRight(TRight fallback)
        {
            return isRight ? right : fallback;
        }

        /// <summary>
        ///     Attempt to unwrap the Right value.
        /// </summary>
        /// <returns>The unwrapped Right value.</returns>
        /// <exception cref="InvalidOperationException">If this Either is Left.</exception>
        public TRight FromRight()
        {
            if (!isRight)
            {
                throw new InvalidOperationException("Either.FromRight: Cannot extract Right from Left");
            }

            return right;
        }

        /// <summary>
        ///     Return a value, depending on whether the Either is Left or Right.
        /// </summary>
        /// <param name="funcLeft">The function to apply to the Left, if it is Left.</param>
        /// <param name="funcRight">The function to apply to the Right, if it is Right.</param>
        /// <typeparam name="TResult">The type of the result of both given functions.</typeparam>
        /// <returns>The result of either <c>funcLeft</c> or <c>funcRight</c>.</returns>
        public TResult FromEither<TResult>(Func<TLeft, TResult> funcLeft, Func<TRight, TResult> funcRight)
        {
            return isRight ? funcRight(right) : funcLeft(left);
        }

        /// <summary>
        ///     Swap the Left and Right side of this Either.
        /// </summary>
        /// <returns>A new Either with Left being this Eithers right and vice versa.</returns>
        public Either<TRight, TLeft> Swap()
        {
            return new Either<TRight, TLeft>(right, left, !isRight);
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return isRight
                ? $"Right {right}"
                : $"Left {left}";
        }
    }
}
