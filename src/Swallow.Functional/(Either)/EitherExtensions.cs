using System.Collections.Generic;
using System.Linq;

namespace Swallow.Functional
{
    /// <summary>
    ///     Useful extension methods regarding Either.
    /// </summary>
    public static class EitherExtensions
    {
        /// <summary>
        ///     Turn a nullable value into an Either - a Left if it is null, a Right if it is not.
        /// </summary>
        /// <param name="value">The value to wrap as Right, if any.</param>
        /// <param name="left">The fallback for Left if <c>value</c> is null.</param>
        /// <typeparam name="TLeft">The type for left.</typeparam>
        /// <typeparam name="TRight">The type for right.</typeparam>
        /// <returns>The resulting Either.</returns>
        public static Either<TLeft, TRight> ToEither<TLeft, TRight>(this TRight value, TLeft left)
            where TRight : class
        {
            return value == null ? Either<TLeft, TRight>.Left(left) : Either<TLeft, TRight>.Right(value);
        }

        /// <summary>
        ///    Return a list of all unwrapped Lefts.
        /// </summary>
        /// <param name="eithers">The eithers to select.</param>
        /// <typeparam name="TLeft">The type of Left.</typeparam>
        /// <typeparam name="TRight">The type of Right.</typeparam>
        /// <returns>A list of all Lefts.</returns>
        public static IEnumerable<TLeft> Lefts<TLeft, TRight>(this IEnumerable<Either<TLeft, TRight>> eithers)
        {
            return eithers.Where(e => e.IsLeft).Select(e => e.FromLeft());
        }

        /// <summary>
        ///    Return a list of all unwrapped Rights.
        /// </summary>
        /// <param name="eithers">The eithers to select.</param>
        /// <typeparam name="TLeft">The type of Left.</typeparam>
        /// <typeparam name="TRight">The type of Right.</typeparam>
        /// <returns>A list of all Rights.</returns>
        public static IEnumerable<TRight> Rights<TLeft, TRight>(this IEnumerable<Either<TLeft, TRight>> eithers)
        {
            return eithers.Where(e => e.IsRight).Select(e => e.FromRight());
        }

        /// <summary>
        ///    Partition a list of Eithers into a list of Lefts and a list of Rights.
        /// </summary>
        /// <param name="eithers">The eithers to select.</param>
        /// <typeparam name="TLeft">The type of Left.</typeparam>
        /// <typeparam name="TRight">The type of Right.</typeparam>
        /// <returns>A tuple of lists of Lefts and Rights.</returns>
        public static (IEnumerable<TLeft> Lefts, IEnumerable<TRight> Rights) Partition<TLeft, TRight>(
            this IEnumerable<Either<TLeft, TRight>> eithers)
        {
            var eitherArray = eithers.ToArray();
            var lefts = eitherArray.Lefts();
            var rights = eitherArray.Rights();

            return (lefts, rights);
        }

        /// <summary>
        ///     Join two levels of Eithers together, producing Right if both are Right.
        /// </summary>
        /// <param name="nested">The nested Either to join.</param>
        /// <typeparam name="TLeft">The type of Left.</typeparam>
        /// <typeparam name="TRight">The type of Right.</typeparam>
        /// <returns>Right if both levels are Right, left otherwise.</returns>
        public static Either<TLeft, TRight> Join<TLeft, TRight>(this Either<TLeft, Either<TLeft, TRight>> nested)
        {
            return nested.Bind(Functional.Identity<Either<TLeft, TRight>>());
        }
    }
}
