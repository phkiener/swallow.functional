namespace Swallow.Functional
{
    /// <summary>
    ///     A type with a single value - similar to void, but able to be instantiated.
    /// </summary>
    public struct Unit
    {
        /// <summary>
        /// Gets the unit value.
        /// </summary>
        public static Unit Value => new Unit();

        /// <inheritdoc />
        public override bool Equals(object obj) => obj is Unit;

        /// <inheritdoc />
        public override int GetHashCode() => 0xBEEF;

        /// <inheritdoc />
        public override string ToString() => "()";
    }
}
