using NUnit.Framework;

namespace Swallow.Functional
{
    [TestFixture]
    public class UnitTest
    {
        [Test]
        public void ToString_Always_ProducesCorrectString()
        {
            // Arrange
            var unit = Unit.Value;

            // Act
            var result = unit.ToString();

            // Assert
            Assert.AreEqual("()", result);
        }

        [Test]
        public void Equals_OtherIsUnit_ReturnsTrue()
        {
            // Arrange
            var unit = Unit.Value;
            var other = Unit.Value;

            // Act
            var result = unit.Equals(other);

            // Assert
            Assert.IsTrue(result);
        }

        [Test]
        public void Equals_OtherIsNotUnit_ReturnsFalse()
        {
            // Arrange
            var unit = Unit.Value;
            var other = "Yo!" as object;

            // Act
            var result = unit.Equals(other);

            // Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void GetHashCode_Always_ReturnsBeef()
        {
            // Act
            var code = Unit.Value.GetHashCode();

            // Assert
            Assert.AreEqual(0xBeef, code);
        }
    }
}
