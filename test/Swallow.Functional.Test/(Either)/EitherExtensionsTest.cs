using System.Collections.Generic;
using NUnit.Framework;

namespace Swallow.Functional
{
    [TestFixture]
    public class EitherExtensionsTest
    {
        [Test]
        public void ToEither_ValueIsNull_ReturnsLeft()
        {
            // Arrange % Act
            var result = (null as string).ToEither(0);

            // Assert
            Assert.IsTrue(result.IsLeft);
            Assert.AreEqual(0, result.FromLeft());
        }

        [Test]
        public void ToEither_ValueIsNotNull_ReturnsRight()
        {
            // Arrange % Act
            var result = "yo".ToEither(0);

            // Assert
            Assert.IsTrue(result.IsRight);
            Assert.AreEqual("yo", result.FromRight());
        }

        [Test]
        public void Lefts_EmptyList_ReturnsEmptyList()
        {
            // Arrange
            var eithers = new Either<string, int>[0];

            // Act
            var lefts = eithers.Lefts();

            // Assert
            Assert.IsEmpty(lefts);
        }

        [Test]
        public void Lefts_OnlyRights_ReturnsEmptyList()
        {
            // Arrange
            var eithers = new List<Either<string, int>>
            {
                Either<string, int>.Right(1),
                Either<string, int>.Right(2)
            };

            // Act
            var lefts = eithers.Lefts();

            // Assert
            Assert.IsEmpty(lefts);
        }

        [Test]
        public void Lefts_OnlyLefts_ReturnsListOfSameLength()
        {
            // Arrange
            var eithers = new List<Either<string, int>>
            {
                Either<string, int>.Left("a"),
                Either<string, int>.Left("b")
            };

            // Act
            var lefts = eithers.Lefts();

            // Assert
            CollectionAssert.AreEqual(new[] { "a", "b" }, lefts);
        }

        [Test]
        public void Lefts_BothLeftsAndRights_ReturnsCorrectList()
        {
            // Arrange
            var eithers = new List<Either<string, int>>
            {
                Either<string, int>.Left("a"),
                Either<string, int>.Right(2),
                Either<string, int>.Left("c")
            };

            // Act
            var lefts = eithers.Lefts();

            // Assert
            CollectionAssert.AreEqual(new[] { "a", "c" }, lefts);
        }

        [Test]
        public void Rights_EmptyList_ReturnsEmptyList()
        {
            // Arrange
            var eithers =  new Either<string, int>[0];

            // Act
            var rights = eithers.Rights();

            // Assert
            Assert.IsEmpty(rights);
        }

        [Test]
        public void Rights_OnlyLefts_ReturnsEmptyList()
        {
            // Arrange
            var eithers = new List<Either<string, int>>
            {
                Either<string, int>.Left("a"),
                Either<string, int>.Left("b")
            };

            // Act
            var rights = eithers.Rights();

            // Assert
            Assert.IsEmpty(rights);
        }

        [Test]
        public void Rights_OnlyRights_ReturnsListOfSameLength()
        {
            // Arrange
            var eithers = new List<Either<string, int>>
            {
                Either<string, int>.Right(1),
                Either<string, int>.Right(2)
            };

            // Act
            var rights = eithers.Rights();

            // Assert
            CollectionAssert.AreEqual(new[] { 1, 2 }, rights);
        }

        [Test]
        public void Rights_BothLeftsAndRights_ReturnsCorrectList()
        {
            // Arrange
            var eithers = new List<Either<string, int>>
            {
                Either<string, int>.Right(1),
                Either<string, int>.Left("b"),
                Either<string, int>.Right(2)
            };

            // Act
            var rights = eithers.Rights();

            // Assert
            CollectionAssert.AreEqual(new[] { 1, 2 }, rights);
        }

        [Test]
        public void Partition_EmptyList_ReturnsTupleOfEmptyLists()
        {
            // Arrange
            var eithers =  new Either<string, int>[0];

            // Act
            var (lefts, rights) = eithers.Partition();

            // Assert
            Assert.IsEmpty(lefts);
            Assert.IsEmpty(rights);
        }

        [Test]
        public void Partition_OnlyLefts_ReturnsCorrectLists()
        {
            // Arrange
            var eithers = new List<Either<string, int>>
            {
                Either<string, int>.Left("b"),
            };

            // Act
            var (lefts, rights) = eithers.Partition();

            // Assert
            CollectionAssert.AreEqual(new[] { "b" }, lefts);
            Assert.IsEmpty(rights);
        }

        [Test]
        public void Partition_OnlyRights_ReturnsCorrectLists()
        {
            // Arrange
            var eithers = new List<Either<string, int>>
            {
                Either<string, int>.Right(1),
                Either<string, int>.Right(2)
            };

            // Act
            var (lefts, rights) = eithers.Partition();

            // Assert
            Assert.IsEmpty(lefts);
            CollectionAssert.AreEqual(new[] { 1, 2 }, rights);
        }

        [Test]
        public void Partition_BothLeftsAndRights_ReturnsCorrectLists()
        {
            // Arrange
            var eithers = new List<Either<string, int>>
            {
                Either<string, int>.Right(1),
                Either<string, int>.Left("b"),
                Either<string, int>.Right(2)
            };

            // Act
            var (lefts, rights) = eithers.Partition();

            // Assert
            CollectionAssert.AreEqual(new[] { "b" }, lefts);
            CollectionAssert.AreEqual(new[] { 1, 2 }, rights);
        }

        [Test]
        public void Join_OuterIsLeft_ReturnsLeft()
        {
            // Arrange
            var nested = Either<string, Either<string, int>>.Left("yo");

            // Arrange
            var result = nested.Join();

            // Assert
            Assert.IsTrue(result.IsLeft);
        }

        [Test]
        public void Join_InnerIsLeft_ReturnsLeft()
        {
            // Arrange
            var nested = Either<string, Either<string, int>>.Right(Either<string, int>.Left("yo"));

            // Arrange
            var result = nested.Join();

            // Assert
            Assert.IsTrue(result.IsLeft);
        }

        [Test]
        public void Join_InnerIsRight_ReturnsRight()
        {
            // Arrange
            var nested = Either<string, Either<string, int>>.Right(Either<string, int>.Right(1));

            // Arrange
            var result = nested.Join();

            // Assert
            Assert.IsTrue(result.IsRight);
        }
    }
}
