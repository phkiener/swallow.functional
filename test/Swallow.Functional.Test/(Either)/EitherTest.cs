using System;
using NUnit.Framework;

namespace Swallow.Functional
{
    [TestFixture]
    public class EitherTest
    {
        [Test]
        public void ToString_Left_ProducesCorrectString()
        {
            // Arrange
            var either = Either<string, int>.Left("yo");

            // Act
            var result = either.ToString();

            // Assert
            Assert.AreEqual("Left yo", result);
        }

        [Test]
        public void ToString_Right_ProducesCorrectString()
        {
            // Arrange
            var either = Either<string, int>.Right(5);

            // Act
            var result = either.ToString();

            // Assert
            Assert.AreEqual("Right 5", result);
        }

        [Test]
        public void FromLeft_EitherIsLeftAndFallbackGiven_ReturnsLeft()
        {
            // Arrange
            var either = Either<string, int>.Left("yo");

            // Act
            var result = either.FromLeft("fallback");

            // Assert
            Assert.AreEqual("yo", result);
        }

        [Test]
        public void FromLeft_EitherIsRightAndFallbackGiven_ReturnsFallback()
        {
            // Arrange
            var either = Either<string, int>.Right(5);

            // Act
            var result = either.FromLeft("fallback");

            // Assert
            Assert.AreEqual("fallback", result);
        }

        [Test]
        public void FromLeft_EitherIsLeft_ReturnsLeft()
        {
            // Arrange
            var either = Either<string, int>.Left("yo");

            // Act
            var result = either.FromLeft();

            // Assert
            Assert.AreEqual("yo", result);
        }

        [Test]
        public void FromLeft_EitherIsRight_ThrowsException()
        {
            // Arrange
            var either = Either<string, int>.Right(5);

            // Act
            var ex = Assert.Throws<InvalidOperationException>(() => either.FromLeft());

            // Assert
            Assert.AreEqual("Either.FromLeft: Cannot extract Left from Right", ex.Message);
        }

        [Test]
        public void FromRight_EitherIsRightAndFallbackGiven_ReturnsRight()
        {
            // Arrange
            var either = Either<string, int>.Right(10);

            // Act
            var result = either.FromRight(5);

            // Assert
            Assert.AreEqual(10, result);
        }

        [Test]
        public void FromRight_EitherIsLeftAndFallbackGiven_ReturnsFallback()
        {
            // Arrange
            var either = Either<string, int>.Left("yo");

            // Act
            var result = either.FromRight(5);

            // Assert
            Assert.AreEqual(5, result);
        }

        [Test]
        public void FromRight_EitherIsRight_ReturnsRight()
        {
            // Arrange
            var either = Either<string, int>.Right(5);

            // Act
            var result = either.FromRight();

            // Assert
            Assert.AreEqual(5, result);
        }

        [Test]
        public void FromRight_EitherIsLeft_ThrowsException()
        {
            // Arrange
            var either = Either<string, int>.Left("yo");

            // Act
            var ex = Assert.Throws<InvalidOperationException>(() => either.FromRight());

            // Assert
            Assert.AreEqual("Either.FromRight: Cannot extract Right from Left", ex.Message);
        }

        [Test]
        public void Map_EitherIsLeft_ReturnsLeft()
        {
            // Arrange
            var either = Either<string, int>.Left("yo");

            // Act
            var result = either.Map(x => x + 1);

            // Assert
            Assert.IsTrue(result.IsLeft);
            Assert.AreEqual("yo", result.FromLeft());
        }

        [Test]
        public void Map_EitherIsRight_ReturnsMappedRight()
        {
            // Arrange
            var either = Either<string, int>.Right(5);

            // Act
            var result = either.Map(x => x + 1);

            // Assert
            Assert.IsTrue(result.IsRight);
            Assert.AreEqual(6, result.FromRight());
        }

        [Test]
        public void Bind_EitherIsLeft_ReturnsLeft()
        {
            // Arrange
            var either = Either<string, int>.Left("yo");

            // Act
            var result = either.Bind(x => Either<string, int>.Right(x + 1));

            // Assert
            Assert.IsTrue(result.IsLeft);
            Assert.AreEqual("yo", result.FromLeft());
        }

        [Test]
        public void Bind_EitherIsRight_ReturnsNewRight()
        {
            // Arrange
            var either = Either<string, int>.Right(5);

            // Act
            var result = either.Bind(x => Either<string, int>.Right(x + 1));

            // Assert
            Assert.IsTrue(result.IsRight);
            Assert.AreEqual(6, result.FromRight());
        }

        [Test]
        public void Then_EitherIsLeft_ReturnsLeft()
        {
            // Arrange
            var either = Either<string, int>.Left("yo");

            // Act
            var result = either.Then(() => Either<string, char>.Right('x'));

            // Assert
            Assert.IsTrue(result.IsLeft);
            Assert.AreEqual("yo", result.FromLeft());
        }

        [Test]
        public void Then_EitherIsRight_ReturnsNewRight()
        {
            // Arrange
            var either = Either<string, int>.Right(5);

            // Act
            var result = either.Then(() => Either<string, char>.Right('x'));

            // Assert
            Assert.IsTrue(result.IsRight);
            Assert.AreEqual('x', result.FromRight());
        }

        [Test]
        public void FromEither_EitherIsLeft_ReturnsResultOfLeftFunc()
        {
            // Arrange
            var either = Either<string, int>.Left("yo");

            // Act
            var result = either.FromEither(l => $"Left was {l}", r => r.ToString());

            // Assert
            Assert.AreEqual("Left was yo", result);
        }

        [Test]
        public void FromEither_EitherIsRight_ReturnsResultOfRightFunc()
        {
            // Arrange
            var either = Either<string, int>.Right(5);

            // Act
            var result = either.FromEither(l => $"Left was {l}", r => r.ToString());

            // Assert
            Assert.AreEqual("5", result);
        }

        [Test]
        public void MapLeft_EitherIsLeft_ReturnsMappedLeft()
        {
            // Arrange
            var either = Either<string, int>.Left("yo");

            // Act
            var result = either.MapLeft(x => $"Some left: {x}");

            // Assert
            Assert.IsTrue(result.IsLeft);
            Assert.AreEqual("Some left: yo", result.FromLeft());
        }

        [Test]
        public void MapLeft_EitherIsRight_ReturnsRight()
        {
            // Arrange
            var either = Either<string, int>.Right(5);

            // Act
            var result = either.MapLeft(x => $"Some left: {x}");

            // Assert
            Assert.IsTrue(result.IsRight);
            Assert.AreEqual(5, result.FromRight());
        }

        [Test]
        public void BindLeft_EitherIsLeft_ReturnsMappedLeft()
        {
            // Arrange
            var either = Either<string, int>.Left("yo");

            // Act
            var result = either.BindLeft(x => Either<string, int>.Left("hello"));

            // Assert
            Assert.IsTrue(result.IsLeft);
            Assert.AreEqual("hello", result.FromLeft());
        }

        [Test]
        public void BindLeft_EitherIsRight_ReturnsRight()
        {
            // Arrange
            var either = Either<string, int>.Right(5);

            // Act
            var result = either.BindLeft(x => Either<string, int>.Left("hello"));

            // Assert
            Assert.IsTrue(result.IsRight);
            Assert.AreEqual(5, result.FromRight());
        }

        [Test]
        public void ThenLeft_EitherIsLeft_ReturnsNewLeft()
        {
            // Arrange
            var either = Either<string, int>.Left("yo");

            // Act
            var result = either.ThenLeft(() => Either<char, int>.Left('x'));

            // Assert
            Assert.IsTrue(result.IsLeft);
            Assert.AreEqual('x', result.FromLeft());
        }

        [Test]
        public void ThenLeft_EitherIsRight_ReturnsRight()
        {
            // Arrange
            var either = Either<string, int>.Right(5);

            // Act
            var result = either.ThenLeft(() => Either<char, int>.Left('x'));

            // Assert
            Assert.IsTrue(result.IsRight);
            Assert.AreEqual(5, result.FromRight());
        }

        [Test]
        public void Swap_EitherIsLeft_ResultingEitherIsRight()
        {
            // Arrange
            var either = Either<string, int>.Left("yo");

            // Act
            var result = either.Swap();

            // Assert
            Assert.IsTrue(result.IsRight);
            Assert.AreEqual("yo", result.FromRight());
        }

        [Test]
        public void Swap_EitherIsRight_ResultingEitherIsLeft()
        {
            // Arrange
            var either = Either<string, int>.Right(5);

            // Act
            var result = either.Swap();

            // Assert
            Assert.IsTrue(result.IsLeft);
            Assert.AreEqual(5, result.FromLeft());
        }
    }
}
