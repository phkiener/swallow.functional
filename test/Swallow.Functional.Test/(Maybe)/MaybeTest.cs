using System;
using NUnit.Framework;

namespace Swallow.Functional
{
    [TestFixture]
    public class MaybeTest
    {
        [Test]
        public void ToString_Nothing_ProducesCorrectString()
        {
            // Arrange
            var maybe = Maybe<int>.Nothing();

            // Act
            var result = maybe.ToString();

            // Assert
            Assert.AreEqual("Nothing", result);
        }

        [Test]
        public void ToString_JustValue_ProducesCorrectString()
        {
            // Arrange
            var maybe = Maybe<int>.Just(5);

            // Act
            var result = maybe.ToString();

            // Assert
            Assert.AreEqual("Just 5", result);
        }

        [Test]
        public void FromJust_MaybeIsJustAndFallbackGiven_ReturnsTheValue()
        {
            // Arrange
            var maybe = Maybe<int>.Just(10);

            // Act
            var result = maybe.FromJust(5);

            // Assert
            Assert.AreEqual(10, result);
        }

        [Test]
        public void FromJust_MaybeIsNothingAndFallbackIsGiven_ReturnsFallback()
        {
            // Arrange
            var maybe = Maybe<int>.Nothing();

            // Act
            var result = maybe.FromJust(5);

            // Assert
            Assert.AreEqual(5, result);
        }

        [Test]
        public void FromJust_MaybeIsJust_ReturnsTheValue()
        {
            // Arrange
            var maybe = Maybe<int>.Just(10);

            // Act
            var result = maybe.FromJust();

            // Assert
            Assert.AreEqual(10, result);
        }

        [Test]
        public void FromJust_MaybeIsNothing_ThrowsException()
        {
            // Arrange
            var maybe = Maybe<int>.Nothing();

            // Act
            var ex = Assert.Throws<InvalidOperationException>(() => maybe.FromJust());

            // Assert
            Assert.AreEqual("Maybe.FromJust: Cannot extract Just from Nothing", ex.Message);
        }

        [Test]
        public void Map_MaybeIsNothing_ReturnsNothing()
        {
            // Arrange
            var maybe = Maybe<int>.Nothing();

            // Act
            var result = maybe.Map(x => x + 1);

            // Assert
            Assert.IsTrue(result.IsNothing);
        }

        [Test]
        public void Map_MaybeIsJust_AppliesFunction()
        {
            // Arrange
            var maybe = Maybe<int>.Just(5);

            // Act
            var result = maybe.Map(x => x + 1);

            // Assert
            Assert.IsTrue(result.IsJust);
            Assert.AreEqual(6, result.FromJust());
        }

        [Test]
        public void Bind_MaybeIsNothing_ReturnsNothing()
        {
            // Arrange
            var maybe = Maybe<int>.Nothing();

            // Act
            var result = maybe.Bind(x => Maybe<string>.Just(x.ToString()));

            // Assert
            Assert.IsTrue(result.IsNothing);
        }

        [Test]
        public void Bind_MaybeIsJust_AppliesFunction()
        {
            // Arrange
            var maybe = Maybe<int>.Just(5);

            // Act
            var result = maybe.Bind(x => Maybe<string>.Just(x.ToString()));

            // Assert
            Assert.IsTrue(result.IsJust);
            Assert.AreEqual("5", result.FromJust());
        }
        [Test]
        public void Then_MaybeIsNothing_ReturnsNothing()
        {
            // Arrange
            var maybe = Maybe<int>.Nothing();

            // Act
            var result = maybe.Then(() => Maybe<string>.Just("yo"));

            // Assert
            Assert.IsTrue(result.IsNothing);
        }

        [Test]
        public void Then_MaybeIsJust_AppliesFunction()
        {
            // Arrange
            var maybe = Maybe<int>.Just(5);

            // Act
            var result = maybe.Then(() => Maybe<string>.Just("yo"));

            // Assert
            Assert.IsTrue(result.IsJust);
            Assert.AreEqual("yo", result.FromJust());
        }
    }
}
