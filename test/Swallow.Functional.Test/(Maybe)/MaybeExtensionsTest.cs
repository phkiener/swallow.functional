using System.Collections.Generic;
using NUnit.Framework;

namespace Swallow.Functional
{
    [TestFixture]
    public class MaybeExtensionsTest
    {
        [Test]
        public void ToMaybe_ValueIsNull_ReturnsNothing()
        {
            // Arrange
            var value = null as string;

            // Act
            var result = value.ToMaybe();

            // Assert
            Assert.IsTrue(result.IsNothing);
        }

        [Test]
        public void ToMaybe_ValueIsNotNull_ReturnsJust()
        {
            // Arrange
            var value = "hello world";

            // Act
            var result = value.ToMaybe();

            // Assert
            Assert.IsTrue(result.IsJust);
            Assert.AreEqual("hello world", result.FromJust());
        }

        [Test]
        public void Justs_EmptyList_ReturnsEmptyList()
        {
            // Arrange
            var list = new Maybe<int>[0];

            // Act
            var justs = list.Justs();

            // Assert
            Assert.IsEmpty(justs);
        }

        [Test]
        public void Justs_OnlyNothings_ReturnsEmptyList()
        {
            // Arrange
            var list = new List<Maybe<int>>
            {
                Maybe<int>.Nothing(),
                Maybe<int>.Nothing()
            };

            // Act
            var justs = list.Justs();

            // Assert
            Assert.IsEmpty(justs);
        }

        [Test]
        public void Justs_OnlyJusts_ReturnsListOfSameLength()
        {
            // Arrange
            var list = new List<Maybe<int>>
            {
                Maybe<int>.Just(5),
                Maybe<int>.Just(6),
                Maybe<int>.Just(7)
            };

            // Act
            var justs = list.Justs();

            // Assert
            CollectionAssert.AreEqual(new[] { 5, 6, 7 }, justs);
        }

        [Test]
        public void Justs_BothNothingsAndJusts_ReturnsUnwrappedJusts()
        {
            // Arrange
            var list = new List<Maybe<int>>
            {
                Maybe<int>.Nothing(),
                Maybe<int>.Just(5),
                Maybe<int>.Nothing(),
                Maybe<int>.Just(7)
            };

            // Act
            var justs = list.Justs();

            // Assert
            CollectionAssert.AreEqual(new[] { 5, 7 }, justs);
        }

        [Test]
        public void Join_OuterIsNothing_ReturnsNothing()
        {
            // Arrange
            var nested = Maybe<Maybe<int>>.Nothing();

            // Arrange
            var result = nested.Join();

            // Assert
            Assert.IsTrue(result.IsNothing);
        }

        [Test]
        public void Join_InnerIsNothing_ReturnsNothing()
        {
            // Arrange
            var nested = Maybe<Maybe<int>>.Just(Maybe<int>.Nothing());

            // Arrange
            var result = nested.Join();

            // Assert
            Assert.IsTrue(result.IsNothing);
        }

        [Test]
        public void Join_InnerIsJust_ReturnsJust()
        {
            // Arrange
            var nested = Maybe<Maybe<int>>.Just(Maybe<int>.Just(5));

            // Arrange
            var result = nested.Join();

            // Assert
            Assert.IsTrue(result.IsJust);
        }
    }
}
