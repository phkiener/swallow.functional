using System;
using System.Linq;
using NUnit.Framework;

namespace Swallow.Functional
{
    [TestFixture]
    public class FunctionalTest
    {
        [Test]
        public void Const_BasicValue_ReturnsValueForAllInputs()
        {
            // Arrange % Act
            var func = Functional.Const<byte, string>("yo");

            // Assert
            Assert.IsTrue(
                Enumerable
                    .Range(byte.MinValue, byte.MaxValue)
                    .Select(x => func((byte)x))
                    .All(s => s == "yo"));
        }

        [Test]
        public void Compose_TwoFunctions_ChainsTogetherCorrectly()
        {
            // Arrange
            Func<int, int> addOne = x => x + 1;

            // Act
            var composed = addOne.Compose(x => x.ToString());

            // Assert
            Assert.AreEqual("12", composed(11));
        }

        [Test]
        public void Identity_ValueGiven_SameValueReturned()
        {
            // Arrange
            var id = Functional.Identity<int>();

            // Act
            var result = id(5);

            // Assert
            Assert.AreEqual(5, result);
        }

        [Test]
        public void Flip_SameType_ProducesCorrectResult()
        {
            // Arrange
            Func<int, int, int> divide = (x, y) => x / y;

            // Act
            var flipped = divide.Flip();

            // Assert
            Assert.AreEqual(5, flipped(20, 100));
            Assert.AreEqual(5, divide(100, 20));
        }

        [Test]
        public void Flip_DifferentTypes_ProducesCorrectResult()
        {
            // Arrange
            Func<string, char, string> append = (s, c) => $"{s}{c}";

            // Act
            var flipped = append.Flip();

            // Assert
            Assert.AreEqual("hello!", flipped('!', "hello"));
            Assert.AreEqual("hello!", append("hello", '!'));
        }

        [Test]
        public void Curry_Always_ReturnsCorrectFunction()
        {
            // Arrange
            Func<int, int, int> sum = (x, y) => x + y;

            // Act
            var curried = sum.Curry();

            // Assert
            Assert.AreEqual(5, curried((1, 4)));
            Assert.AreEqual(5, sum(1, 4));
        }

        [Test]
        public void Uncurry_Always_ReturnsCorrectFunction()
        {
            // Arrange
            Func<(int, int), int> sum = tuple => tuple.Item1 + tuple.Item2;

            // Act
            var uncurried = sum.Uncurry();

            // Assert
            Assert.AreEqual(5, uncurried(1, 4));
            Assert.AreEqual(5, sum((1, 4)));
        }

        [Test]
        public void Do_NoArgument_ActionIsInvokedAndUnitReturnedWhenFunctionCalled()
        {
            // Arrange
            var methodCalled = false;

            // Act
            var func = Functional.Do(() => methodCalled = true);
            var result = func.Invoke();

            // Assert
            Assert.AreEqual(Unit.Value, result);
            Assert.IsTrue(methodCalled);
        }

        [Test]
        public void Do_WithArgument_ActionIsInvokedAndUnitReturnedWhenFunctionCalled()
        {
            // Arrange
            var param = null as int?;

            // Act
            var func = Functional.Do((int x) => param = x);
            var result = func.Invoke(5);

            // Assert
            Assert.AreEqual(Unit.Value, result);
            Assert.AreEqual(5, param ?? 0);
        }

        [Test]
        public void Apply_TwoArgumentsAndOneApplied_ReturnsCorrectResultWhenCalled()
        {
            // Arrange
            Func<int, int, int> function = (x, y) => x + y;

            // Act
            var partiallyApplied = function.Apply(1);

            // Assert
            Assert.AreEqual(5, partiallyApplied(4));
        }

        [Test]
        public void Apply_ThreeArgumentsAndOneApplied_ReturnsCorrectResultWhenCalled()
        {
            // Arrange
            Func<int, int, int, int> function = (x, y, z) => x + y + z;

            // Act
            var partiallyApplied = function.Apply(1);

            // Assert
            Assert.AreEqual(10, partiallyApplied(4, 5));
        }

        [Test]
        public void Apply_ThreeArgumentsAndTwoApplied_ReturnsCorrectResultWhenCalled()
        {
            // Arrange
            Func<int, int, int, int> function = (x, y, z) => x + y + z;

            // Act
            var partiallyApplied = function.Apply(1, 4);

            // Assert
            Assert.AreEqual(10, partiallyApplied(5));
        }

        [Test]
        public void Apply_FourArgumentsAndOneApplied_ReturnsCorrectResultWhenCalled()
        {
            // Arrange
            Func<int, int, int, int, int> function = (x, y, z, w) => x + y + z + w;

            // Act
            var partiallyApplied = function.Apply(1);

            // Assert
            Assert.AreEqual(20, partiallyApplied(4, 5, 10));
        }

        [Test]
        public void Apply_FourArgumentsAndTwoApplied_ReturnsCorrectResultWhenCalled()
        {
            // Arrange
            Func<int, int, int, int, int> function = (x, y, z, w) => x + y + z + w;

            // Act
            var partiallyApplied = function.Apply(1, 4);

            // Assert
            Assert.AreEqual(20, partiallyApplied(5, 10));
        }

        [Test]
        public void Apply_FourArgumentsAndThreeApplied_ReturnsCorrectResultWhenCalled()
        {
            // Arrange
            Func<int, int, int, int, int> function = (x, y, z, w) => x + y + z + w;

            // Act
            var partiallyApplied = function.Apply(1, 4, 5);

            // Assert
            Assert.AreEqual(20, partiallyApplied(10));
        }
    }
}
