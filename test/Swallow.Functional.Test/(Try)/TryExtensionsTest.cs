using System;
using NUnit.Framework;

namespace Swallow.Functional
{
    [TestFixture]
    public class TryExtensionsTest
    {
        [Test]
        [TestCase("bla", true)]
        [TestCase("123", false)]
        public void Catch_FunctionWithParameter_HandlesExceptionCorrectly(string value, bool shouldThrow)
        {
            // Arrange
            Func<string, int> parseInt = int.Parse;

            // Act
            var safeFunc = parseInt.Catch();
            var result = safeFunc(value);

            // Assert
            if (shouldThrow)
            {
                Assert.IsTrue(result.IsException);
                Assert.IsTrue(result.FromException() is FormatException);
            }
            else
            {
                Assert.IsTrue(result.IsSuccess);
                Assert.AreEqual(123, result.FromSuccess());
            }
        }

        [Test]
        public void Catch_FunctionWithoutParameterThatDoesNotThrow_ReturnsCorrectResult()
        {
            // Arrange
            Func<int> parseInt = () => 5;

            // Act
            var safeFunc = parseInt.Catch();
            var result = safeFunc();

            // Assert
            Assert.IsTrue(result.IsSuccess);
            Assert.AreEqual(5, result.FromSuccess());
        }

        [Test]
        public void Catch_FunctionWithoutParameterThatThrows_ReturnsCorrectResult()
        {
            // Arrange
            Func<int> parseInt = () => throw new FormatException();

            // Act
            var safeFunc = parseInt.Catch();
            var result = safeFunc();

            // Assert
            Assert.IsTrue(result.IsException);
            Assert.IsTrue(result.FromException() is FormatException);
        }

        [Test]
        public void Catch_FunctionWithParameterAndValueThatDoesNotThrow_ReturnsCorrectResult()
        {
            // Arrange
            Func<string, int> parseInt = int.Parse;

            // Act
            var result = parseInt.Catch("5");

            // Assert
            Assert.IsTrue(result.IsSuccess);
            Assert.AreEqual(5, result.FromSuccess());
        }

        [Test]
        public void Catch_FunctionWithParameterAndValueThatThatThrows_ReturnsCorrectResult()
        {
            // Arrange
            Func<string, int> parseInt = int.Parse;

            // Act
            var result = parseInt.Catch("bla");

            // Assert
            Assert.IsTrue(result.IsException);
            Assert.IsTrue(result.FromException() is FormatException);
        }
    }
}
