using System;
using NUnit.Framework;

namespace Swallow.Functional
{
    [TestFixture]
    public class TryTest
    {
        [Test]
        public void ToString_Success_ProducesCorrectString()
        {
            // Arrange
            var t = Try<int>.Success(5);

            // Act
            var result = t.ToString();

            // Assert
            Assert.AreEqual("Success 5", result);
        }

        [Test]
        public void ToString_Exception_ProducesCorrectString()
        {
            // Arrange
            var t = Try<int>.Exception(new ArgumentException());

            // Act
            var result = t.ToString();

            // Assert
            Assert.AreEqual("Exception Value does not fall within the expected range.", result);
        }

        [Test]
        public void FromSuccess_NoFallbackAndTryIsSuccess_ReturnsValue()
        {
            // Arrange
            var t = Try<int>.Success(5);

            // Act
            var result = t.FromSuccess();

            // Assert
            Assert.AreEqual(5, result);
        }

        [Test]
        public void FromSuccess_NoFallbackAndTryIsException_ThrowsException()
        {
            // Arrange
            var t = Try<int>.Exception(new ArgumentException());

            // Act % Assert
            var ex = Assert.Throws<InvalidOperationException>(() => t.FromSuccess());

            // Assert
            Assert.AreEqual("Try.FromSuccess: Cannot extract Success from Exception", ex.Message);
        }

        [Test]
        public void FromSuccess_WithFallbackAndTryIsSuccess_ReturnsValue()
        {
            // Arrange
            var t = Try<int>.Success(5);

            // Act
            var result = t.FromSuccess(15);

            // Assert
            Assert.AreEqual(5, result);
        }

        [Test]
        public void FromSuccess_WithFallbackAndTryIsException_ReturnsFallback()
        {
            // Arrange
            var t = Try<int>.Exception(new ArgumentException());

            // Act
            var result = t.FromSuccess(15);

            // Assert
            Assert.AreEqual(15, result);
        }

        [Test]
        public void FromException_NoFallbackAndTryIsSuccess_ThrowsException()
        {
            // Arrange
            var t = Try<int>.Success(5);

            // Act % Assert
            var ex = Assert.Throws<InvalidOperationException>(() => t.FromException());

            // Assert
            Assert.AreEqual("Try.FromException: Cannot extract Exception from Success", ex.Message);
        }

        [Test]
        public void FromException_NoFallbackAndTryIsException_ReturnsValue()
        {
            // Arrange
            var t = Try<int>.Exception(new ArgumentException());

            // Act
            var result = t.FromException();

            // Assert
            Assert.IsTrue(result is ArgumentException);
        }

        [Test]
        public void FromException_WithFallbackAndTryIsSuccess_ReturnsFallback()
        {
            // Arrange
            var t = Try<int>.Success(5);

            // Act
            var result = t.FromException(new InvalidOperationException());

            // Assert
            Assert.IsTrue(result is InvalidOperationException);
        }

        [Test]
        public void FromException_WithFallbackAndTryIsException_ReturnsValue()
        {
            // Arrange
            var t = Try<int>.Exception(new ArgumentException());

            // Act
            var result = t.FromException(new InvalidOperationException());

            // Assert
            Assert.IsTrue(result is ArgumentException);
        }

        [Test]
        public void Map_TryIsSuccess_MapsFunctionOnValue()
        {
            // Arrange
            var t = Try<int>.Success(5);

            // Act
            var result = t.Map(x => x + 1);

            // Assert
            Assert.IsTrue(result.IsSuccess);
            Assert.AreEqual(6, result.FromSuccess());
        }

        [Test]
        public void Map_TryIsException_ReturnsExceptionAgain()
        {
            // Arrange
            var t = Try<int>.Exception(new ArgumentException());

            // Act
            var result = t.Map(x => x + 1);

            // Assert
            Assert.IsTrue(result.IsException);
            Assert.IsTrue(result.FromException() is ArgumentException);
        }

        [Test]
        public void Bind_TryIsSuccess_FunctionsResultIsReturned()
        {
            // Arrange
            var t = Try<int>.Success(5);

            // Act
            var result = t.Bind(x => Try<int>.Success(x + 1));

            // Assert
            Assert.IsTrue(result.IsSuccess);
            Assert.AreEqual(6, result.FromSuccess());
        }

        [Test]
        public void Bind_TryIsException_ReturnsExceptionAgain()
        {
            // Arrange
            var t = Try<int>.Exception(new ArgumentException());

            // Act
            var result = t.Bind(x => Try<int>.Success(x + 1));

            // Assert
            Assert.IsTrue(result.IsException);
            Assert.IsTrue(result.FromException() is ArgumentException);
        }

        [Test]
        public void Bind_TryIsSuccessAndFunctionThrows_ReturnsThrownException()
        {
            // Arrange
            var t = Try<int>.Success(5);

            // Act
            var result = t.Bind(x => x == 0 ? x : throw new ApplicationException());

            // Assert
            Assert.IsTrue(result.IsException);
            Assert.IsTrue(result.FromException() is ApplicationException);
        }

        [Test]
        public void Bind_TryIsExceptionAndFunctionWouldThrow_ReturnsOriginalException()
        {
            // Arrange
            var t = Try<int>.Exception(new ArgumentException());

            // Act
            var result = t.Bind(x => x == 0 ? x : throw new ApplicationException());

            // Assert
            Assert.IsTrue(result.IsException);
            Assert.IsTrue(result.FromException() is ArgumentException);
        }

        [Test]
        public void Then_TryIsSuccess_FunctionsResultIsReturned()
        {
            // Arrange
            var t = Try<int>.Success(5);

            // Act
            var result = t.Then(() => Try<int>.Success(6));

            // Assert
            Assert.IsTrue(result.IsSuccess);
            Assert.AreEqual(6, result.FromSuccess());
        }

        [Test]
        public void Then_TryIsException_ReturnsExceptionAgain()
        {
            // Arrange
            var t = Try<int>.Exception(new ArgumentException());

            // Act
            var result = t.Then(() => Try<int>.Success(6));

            // Assert
            Assert.IsTrue(result.IsException);
            Assert.IsTrue(result.FromException() is ArgumentException);
        }

        [Test]
        public void Then_TryIsSuccessAndFunctionThrows_ReturnsThrownException()
        {
            // Arrange
            var t = Try<int>.Success(5);

            // Act
            var result = t.Then(FunctionThatAlwaysThrows);

            // Assert
            Assert.IsTrue(result.IsException);
            Assert.IsTrue(result.FromException() is ApplicationException);
        }

        [Test]
        public void Then_TryIsExceptionAndFunctionWouldThrow_ReturnsOriginalException()
        {
            // Arrange
            var t = Try<int>.Exception(new ArgumentException());

            // Act
            var result = t.Then(FunctionThatAlwaysThrows);

            // Assert
            Assert.IsTrue(result.IsException);
            Assert.IsTrue(result.FromException() is ArgumentException);
        }

        private static int FunctionThatAlwaysThrows()
        {
            throw new ApplicationException();
        }
    }
}
