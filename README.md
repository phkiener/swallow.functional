# Swallow.Functional

A small glimpse of functional programming for C#. This includes implementations of the `Maybe` and
`Either` functors, as well as some helpers and extensions regarding functions.

This library is heavily influenced by Haskell. If you know your way around haskell, you might see
lots of familiar stuff.

## Maybe

`Maybe` is a type that can either contain a value wrapped as `Just` or be `Nothing`, i.e. a safer
null.
This is a way to work around the
[Billion Dollar Mistake](https://www.infoq.com/presentations/Null-References-The-Billion-Dollar-Mistake-Tony-Hoare/).

Once you have a `Maybe`, either via explicit static functions on the type or via the `ToMaybe`
extension, you can act on the maybe with `Then`, `Bind` and `Map`, modifying the `Just` but
leaving the `Nothing` untouched.

Unwrapping is allowed but should not be used primarily; by using `FromJust`, you can return the
`Just`-value or either return a fallback or throw an exception.

### Example

```C#
var result = SomeCallThatCanReturnNull()
    .ToMaybe()
    .Map(x => SomeFunction(x))
    .Bind(x => SomeFunctionThatReturnsMaybe(x));
```

## Either

`Either` is similar to maybe, but instead of having just one yes-or-no, you have two possible values. An
`Either` can either be `Left` - which is generally used in case of errors - or a `Right` - which is
generally used as success.

The usage is very similar to `Maybe` with `Map`, `Bind` and `Then`, but you also have `MapLeft` to
map the `Left` value. This is needed, because `Bind` and `Then` require the result's left to be
the same as the left of the either that is acted on. You can either create an `Either` explicitly
or via the extension `ToEither`, which is very similar to `ToMaybe`.

The same rule for unwrapping applies as for `Maybe`, but there is a generally safer version, too:
`FromEither` takes in two functions to coalesce both left and right to one type, resulting in a
safe operation.

### Example

```C#
var result = SomeCallThatCanReturnNull()
    .ToEither<string, int>()
    .Map(x => SomeFunction(x))
    .Bind(x => SomeFunctionThatReturnsMaybe(x))
    .FromEither(_ => 0, right => right);
```

## Try

`Try` is again similar to `Either`, in that it is one of two possible values. Essentially, a `Try`
encapsulates a throwing operation in a safe way, unmaking all progress in stack unwinding ever made.

The usage is the same as well - `Map`, `Bind` and `Then`. For convenience, `Then` and `Bind` also
provide auto-lifting; they take in normal functions that throw exceptions and catch these.

Unwrapping is done via `FromSuccess` and `FromException`.

### Example

```C#
var result = FunctionThatThrows.Catch("value")
    .Map(x => x * 2)
    .Bind(x => CallThatReturnsTry(x))
    .Then(() => FunctionThatThrows);
```
